Quintiq file version 2.0
{
  viewcontents
  {
    forms
    {
      form_FormPlanning
      {
        title: 'FormPlanning'
        shown: true
        componentID: 'FormPlanning'
        layout
        {
          mode: 'open'
          rowPosition: 11
          rowSpan: 10
          columnPosition: 1
          columnSpan: 12
        }
        components
        {
          FormPlanning_GanttChartOrders
          {
            RowHeaderWidth: 250
            SynchronizationGroup: 'Planning'
          }
        }
      }
      form_FormChart
      {
        title: 'FormChart'
        shown: true
        componentID: 'FormChart'
        layout
        {
          mode: 'open'
          rowPosition: 1
          rowSpan: 10
          columnPosition: 1
          columnSpan: 12
        }
        components
        {
          FormChart_ChartOrders
          {
            SynchronizationGroup: ''
          }
        }
      }
    }
    userconfigurableinformation
    {
    }
  }
  formatversion: 2
  id: 'Plan Dashboard'
  name: 'Plan Dashboard'
  isglobal: false
  isroot: true
}
