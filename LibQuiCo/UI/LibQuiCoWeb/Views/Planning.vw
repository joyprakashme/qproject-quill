Quintiq file version 2.0
{
  viewcontents
  {
    forms
    {
      form_FormPlanning
      {
        title: 'FormPlanning'
        shown: true
        componentID: 'FormPlanning'
        layout
        {
          mode: 'open'
          rowPosition: 11
          rowSpan: 10
          columnPosition: 1
          columnSpan: 12
        }
        components
        {
          FormPlanning_GanttChartOrders
          {
            RowHeaderWidth: 250
            SynchronizationGroup: 'Planning'
          }
        }
      }
      form_FormOrders
      {
        title: 'FormOrders'
        shown: true
        componentID: 'FormOrders'
        layout
        {
          mode: 'open'
          rowPosition: 1
          rowSpan: 10
          columnPosition: 1
          columnSpan: 8
        }
        components
        {
          FormOrders_PanelOrders
          {
            sizeRatio: 1.22613064723534
          }
          FormOrders_ListOrders
          {
          }
          FormOrders_DataSetLevel859
          {
            groupDepth: -1
            column_ProducedInTime
            {
              columnId: 'ProducedInTime'
              dataPath: 'ProducedInTime'
              dataType: 'boolean'
              title: 'ProducedInTime'
              index: 0
              subtotals: ''
              width: 55
            }
            column_OrderNr
            {
              columnId: 'OrderNr'
              dataPath: 'OrderNr'
              dataType: 'number'
              title: 'OrderNr'
              index: 1
              subtotals: ''
              width: 71
            }
            column_DueDate
            {
              columnId: 'DueDate'
              dataPath: 'DueDate'
              dataType: 'date'
              title: 'DueDate'
              index: 2
              subtotals: ''
              width: 101
            }
            column_Quantity
            {
              columnId: 'Quantity'
              dataPath: 'Quantity'
              dataType: 'number'
              title: 'Quantity'
              index: 3
              subtotals: ''
              width: 74
            }
            column_ERPArticle_ArticleNo
            {
              columnId: 'ERPArticle.ArticleNo'
              dataPath: 'ERPArticle.ArticleNo'
              dataType: 'string'
              title: 'ArticleNo (ERPArticle)'
              index: 4
              subtotals: ''
              width: 90
            }
            column_ERPArticle_Length
            {
              columnId: 'ERPArticle.Length'
              dataPath: 'ERPArticle.Length'
              dataType: 'number'
              title: 'Length (ERPArticle)'
              index: 5
              subtotals: ''
              width: 81
            }
            column_ERPArticle_Width
            {
              columnId: 'ERPArticle.Width'
              dataPath: 'ERPArticle.Width'
              dataType: 'real'
              title: 'Width (ERPArticle)'
              index: 6
              subtotals: ''
              width: 73
            }
          }
          FormOrders_PanelOperations
          {
            sizeRatio: 0.773869352764658
          }
          FormOrders_ListOperations
          {
          }
          FormOrders_DataSetLevelOperations
          {
            groupDepth: -1
            sort: 'OperationType'
            column_OperationType
            {
              columnId: 'OperationType'
              dataPath: 'OperationType'
              dataType: 'string'
              title: 'OperationType'
              index: 0
              subtotals: ''
              width: 150
            }
            column_EarliestStart
            {
              columnId: 'EarliestStart'
              dataPath: 'EarliestStart'
              dataType: 'datetime'
              title: 'EarliestStart'
              index: 1
              subtotals: ''
              width: 150
            }
            column_LatestEnd
            {
              columnId: 'LatestEnd'
              dataPath: 'LatestEnd'
              dataType: 'datetime'
              title: 'LatestEnd'
              index: 2
              subtotals: ''
              width: 150
            }
            column_Task_Machine_Name
            {
              columnId: 'Task.Machine.Name'
              dataPath: 'Task.Machine.Name'
              dataType: 'string'
              title: 'Name (Task.Machine)'
              index: 3
              subtotals: ''
              width: 150
            }
            column_Task_Start
            {
              columnId: 'Task.Start'
              dataPath: 'Task.Start'
              dataType: 'datetime'
              title: 'Start (Task)'
              index: 4
              subtotals: ''
              width: 150
            }
            column_Task_End
            {
              columnId: 'Task.End'
              dataPath: 'Task.End'
              dataType: 'datetime'
              title: 'End (Task)'
              index: 5
              subtotals: ''
              width: 150
            }
          }
        }
      }
      form_FormMachines
      {
        title: 'FormMachines'
        shown: true
        componentID: 'FormMachines'
        layout
        {
          mode: 'open'
          rowPosition: 1
          rowSpan: 10
          columnPosition: 9
          columnSpan: 4
        }
        components
        {
          FormMachines_List345
          {
          }
          FormMachines_DataSetLevelMachineGroup
          {
            groupDepth: -1
            sort: 'Name'
            column_Name
            {
              columnId: 'Name'
              dataPath: 'Name'
              dataType: 'string'
              title: 'Name'
              index: 0
              subtotals: ''
              width: 84
            }
            column_OperationType
            {
              columnId: 'OperationType'
              dataPath: 'OperationType'
              dataType: 'string'
              title: 'OperationType'
              index: 1
              subtotals: ''
              width: 150
            }
          }
          FormMachines_DataSetLevelMachine
          {
            groupDepth: -1
            column_MachineGroup_ImgOperationType
            {
              columnId: 'MachineGroup.ImgOperationType'
              dataPath: 'MachineGroup.ImgOperationType'
              dataType: 'string'
              title: 'ImgOperationType (MachineGroup)'
              index: 0
              subtotals: ''
              width: 78
            }
            column_Name
            {
              columnId: 'Name'
              dataPath: 'Name'
              dataType: 'string'
              title: 'Name'
              index: 1
              subtotals: ''
              width: 70
            }
            column_Speed
            {
              columnId: 'Speed'
              dataPath: 'Speed'
              dataType: 'number'
              title: 'Speed'
              index: 2
              subtotals: ''
              width: 73
            }
            column_MaximumWidth
            {
              columnId: 'MaximumWidth'
              dataPath: 'MaximumWidth'
              dataType: 'real'
              title: 'MaximumWidth'
              index: 3
              subtotals: ''
              width: 150
            }
          }
        }
      }
    }
    userconfigurableinformation
    {
    }
  }
  formatversion: 2
  id: 'Planning'
  name: 'Planning'
  isglobal: false
  isroot: true
}
