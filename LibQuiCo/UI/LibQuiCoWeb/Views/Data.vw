Quintiq file version 2.0
{
  viewcontents
  {
    forms
    {
      form_FormOrders
      {
        title: 'FormOrders'
        shown: true
        componentID: 'FormOrders'
        layout
        {
          mode: 'open'
          rowPosition: 1
          rowSpan: 20
          columnPosition: 1
          columnSpan: 8
        }
        components
        {
          FormOrders_PanelOrders
          {
            sizeRatio: 1.23700870229177
          }
          FormOrders_ListOrders
          {
          }
          FormOrders_DataSetLevel859
          {
            groupDepth: -1
            column_ProducedInTime
            {
              columnId: 'ProducedInTime'
              dataPath: 'ProducedInTime'
              dataType: 'boolean'
              title: 'ProducedInTime'
              index: 0
              subtotals: ''
              width: 38
            }
            column_OrderNr
            {
              columnId: 'OrderNr'
              dataPath: 'OrderNr'
              dataType: 'number'
              title: 'OrderNr'
              index: 1
              subtotals: ''
              width: 75
            }
            column_DueDate
            {
              columnId: 'DueDate'
              dataPath: 'DueDate'
              dataType: 'date'
              title: 'DueDate'
              index: 2
              subtotals: ''
              width: 98
            }
            column_Quantity
            {
              columnId: 'Quantity'
              dataPath: 'Quantity'
              dataType: 'number'
              title: 'Quantity'
              index: 3
              subtotals: ''
              width: 72
            }
            column_ERPArticle_ArticleNo
            {
              columnId: 'ERPArticle.ArticleNo'
              dataPath: 'ERPArticle.ArticleNo'
              dataType: 'string'
              title: 'ArticleNo (ERPArticle)'
              index: 4
              subtotals: ''
              width: 91
            }
            column_ERPArticle_Length
            {
              columnId: 'ERPArticle.Length'
              dataPath: 'ERPArticle.Length'
              dataType: 'number'
              title: 'Length (ERPArticle)'
              index: 5
              subtotals: ''
              width: 81
            }
            column_ERPArticle_Width
            {
              columnId: 'ERPArticle.Width'
              dataPath: 'ERPArticle.Width'
              dataType: 'real'
              title: 'Width (ERPArticle)'
              index: 6
              subtotals: ''
              width: 73
            }
          }
          FormOrders_PanelOperations
          {
            sizeRatio: 0.762991297708232
          }
          FormOrders_ListOperations
          {
          }
          FormOrders_DataSetLevelOperations
          {
            groupDepth: -1
            column_OperationType
            {
              columnId: 'OperationType'
              dataPath: 'OperationType'
              dataType: 'string'
              title: 'OperationType'
              index: 0
              subtotals: ''
              width: 150
            }
            column_EarliestStart
            {
              columnId: 'EarliestStart'
              dataPath: 'EarliestStart'
              dataType: 'datetime'
              title: 'EarliestStart'
              index: 1
              subtotals: ''
              width: 150
            }
            column_LatestEnd
            {
              columnId: 'LatestEnd'
              dataPath: 'LatestEnd'
              dataType: 'datetime'
              title: 'LatestEnd'
              index: 2
              subtotals: ''
              width: 150
            }
            column_Task_Machine_Name
            {
              columnId: 'Task.Machine.Name'
              dataPath: 'Task.Machine.Name'
              dataType: 'string'
              title: 'Name (Task.Machine)'
              index: 3
              subtotals: ''
              width: 150
            }
            column_Task_Start
            {
              columnId: 'Task.Start'
              dataPath: 'Task.Start'
              dataType: 'datetime'
              title: 'Start (Task)'
              index: 4
              subtotals: ''
              width: 150
            }
            column_Task_End
            {
              columnId: 'Task.End'
              dataPath: 'Task.End'
              dataType: 'datetime'
              title: 'End (Task)'
              index: 5
              subtotals: ''
              width: 150
            }
          }
        }
      }
      form_FormMachines
      {
        title: 'FormMachines'
        shown: true
        componentID: 'FormMachines'
        layout
        {
          mode: 'open'
          rowPosition: 1
          rowSpan: 8
          columnPosition: 9
          columnSpan: 12
        }
        components
        {
          FormMachines_List345
          {
          }
          FormMachines_DataSetLevelMachineGroup
          {
            groupDepth: -1
            column_Name
            {
              columnId: 'Name'
              dataPath: 'Name'
              dataType: 'string'
              title: 'Name'
              index: 0
              subtotals: ''
              width: 85
            }
            column_OperationType
            {
              columnId: 'OperationType'
              dataPath: 'OperationType'
              dataType: 'string'
              title: 'OperationType'
              index: 1
              subtotals: ''
              width: 150
            }
          }
          FormMachines_DataSetLevelMachine
          {
            groupDepth: -1
            column_MachineGroup_ImgOperationType
            {
              columnId: 'MachineGroup.ImgOperationType'
              dataPath: 'MachineGroup.ImgOperationType'
              dataType: 'string'
              title: 'MachineGroup.ImgOperationType'
              index: 0
              subtotals: ''
              width: 84
            }
            column_Name
            {
              columnId: 'Name'
              dataPath: 'Name'
              dataType: 'string'
              title: 'Name'
              index: 1
              subtotals: ''
              width: 150
            }
            column_Speed
            {
              columnId: 'Speed'
              dataPath: 'Speed'
              dataType: 'number'
              title: 'Speed'
              index: 2
              subtotals: ''
              width: 78
            }
            column_MaximumWidth
            {
              columnId: 'MaximumWidth'
              dataPath: 'MaximumWidth'
              dataType: 'real'
              title: 'MaximumWidth'
              index: 3
              subtotals: ''
              width: 121
            }
          }
        }
      }
      form_FormERPArticles
      {
        title: 'FormERPArticles'
        shown: true
        componentID: 'FormERPArticles'
        layout
        {
          mode: 'open'
          rowPosition: 9
          rowSpan: 12
          columnPosition: 9
          columnSpan: 5
        }
        components
        {
          FormERPArticles_ListERPArticles
          {
          }
          FormERPArticles_DataSetLevelERPArticles
          {
            groupDepth: -1
            column_ArticleNo
            {
              columnId: 'ArticleNo'
              dataPath: 'ArticleNo'
              dataType: 'string'
              title: 'ArticleNo'
              index: 0
              subtotals: ''
              width: 150
            }
            column_Length
            {
              columnId: 'Length'
              dataPath: 'Length'
              dataType: 'number'
              title: 'Length'
              index: 1
              subtotals: ''
              width: 150
            }
            column_Width
            {
              columnId: 'Width'
              dataPath: 'Width'
              dataType: 'real'
              title: 'Width'
              index: 2
              subtotals: ''
              width: 150
            }
          }
        }
      }
    }
    userconfigurableinformation
    {
    }
  }
  formatversion: 2
  id: 'Data'
  name: 'Data'
  isglobal: false
  isroot: true
}
