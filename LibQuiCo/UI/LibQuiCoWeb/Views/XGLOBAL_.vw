Quintiq file version 2.0
{
  viewcontents
  {
    config
    {
      dockLeftMode: false
      dockRightMode: false
      dockLeftLabel: false
      dockRightLabel: false
      dockLeftCollapsed: false
      dockRightCollapsed: false
    }
    application
    {
      abActionBar
      {
        isBottom: false
        isCompact: false
        isFloating: false
        isHiding: false
        selectedPage: 'applicationConfigureActionBarPageDef'
      }
      applicationDataActionBarPageDef_abgDataGeneral_pnlDataGeneral
      {
        sizeRatio: 1
      }
      applicationFixedPageActionBarPageDef_abgStandardGroup_pnlStandardAction
      {
        sizeRatio: 1
      }
      applicationFixedPageActionBarPageDef_abgStandardGroup_pnlStandardAction2
      {
        sizeRatio: 1
      }
      applicationFixedPageActionBarPageDef_abgStandardGroup_pnlStandardAction3
      {
        sizeRatio: 1
      }
      applicationFixedPageActionBarPageDef_abgStandardGroup_pnlStandardInformation
      {
        sizeRatio: 1
      }
    }
    forms
    {
      form_FormDashboard
      {
        title: 'Dashboard'
        shown: true
        componentID: 'FormDashboard'
        layout
        {
          mode: 'dockleft'
          index: 0
        }
        components: null
      }
      form_FormFactoryOverview
      {
        title: 'Factory overview'
        shown: false
        componentID: 'FormFactoryOverview'
        layout
        {
          mode: 'dockleft'
          index: 1
        }
        components
        {
          FormFactoryOverview_CustomDrawFactoryOverview
          {
            forceFitY: false
            zoomX: 1.25
            zoomY: 1.25
            forceFitX: true
            backendState
            {
              componentId: 'QLibQuiCo::FormFactoryOverview.CustomDrawFactoryOverview'
              state
              {
              }
            }
          }
        }
      }
    }
    userconfigurableinformation
    {
    }
  }
  formatversion: 2
  id: '[GLOBAL]'
  name: '[GLOBAL]'
  isglobal: true
  isroot: true
}
