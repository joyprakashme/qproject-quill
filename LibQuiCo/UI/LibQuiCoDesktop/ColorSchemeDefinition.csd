Quintiq file version 2.0
#parent: LibQuiCoDesktop
ColorSchemeDefinition
{
  Entry
  {
    #keys: '1[10154.0.541170]'
    DefaultColorSerializationOnly: Yellow
    Description: 'The color of the setuptime of the task.'
    Name: 'TaskSetupColor'
  }
  Entry
  {
    #keys: '1[137682.0.759440708]'
    DefaultColorSerializationOnly: '$800080'
    Name: 'NextOperationHighlighting'
  }
}
