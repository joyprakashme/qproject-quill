Quintiq file version 2.0
#parent: #root
Method SynchronizeCalendars (LibCal_ImportExport importExport_i, String context_i, Boolean doDelete_i, 
  Boolean keepIfNotImported_i)
{
  TextBody:
  [*
    synchronizedCalendars := construct( LibCal_Calendars );
    
    LibCal_Util::Info( "#StagingCalendars = " + [String]this.StagingCalendar( relsize ) );
    
    // Calendars that have participants must be synchronized first.
    sortedStagingCalendars := selectsortedset( this, StagingCalendar, stagCal, true, not stagCal.HasParticipants() );  // Use 'not' to have the TRUEs first.
    traverse( sortedStagingCalendars, Elements, stagCal )
    {
      // Find the calendar, create a new one if necessary.
      calendar := importExport_i.FindCreateCalendar( stagCal );
      
      if( not isnull( calendar ) )
      {
        calendar.Synchronize( stagCal, doDelete_i, keepIfNotImported_i );
        synchronizedCalendars.Add( calendar );
      }
    }
    
    // Update the calendars that have been synchronized.
    traverse( synchronizedCalendars, Elements, calendar )
    {
      calendar.GenerateTimeIntervals();
    }
    
    // If applicable, delete the calendars that were not present in the import.
    if( doDelete_i )
    {
      importExport_i.DeleteNotImportedCalendars( synchronizedCalendars, context_i, keepIfNotImported_i );
    }
  *]
}
