Quintiq file version 2.0
#parent: #root
Method SetRecurrencePeriodWithEndDate (Date startDate_i, Date endDate_i)
{
  Description: 'Set a recurrence period that starts at a specific date and ends at a specific date.'
  TextBody:
  [*
    // Check if a new RecurrencePeriod must be created.
    period       := guard( this.RecurrencePeriod().astype( LibCal_RecurrencePeriodWithEndDate ), null( LibCal_RecurrencePeriodWithEndDate ) );
    changeOfType := isnull( period );
    
    isChanged := changeOfType  // There was no period yet, or it was not a PeriodWithoutEnd
              or period.StartDate() <> startDate_i
              or period.EndDate()   <> endDate_i;
    
    if( isChanged )
    {
      // Create the RecurrencePeriod.
      LibCal_RecurrencePeriodWithEndDate::Create( this, startDate_i, endDate_i );
      this.IsChanged( true );
    
      // If there was a change in type of RecurrencePeriod, then also update the subscribers that use the period of the LeadingParticipation.
      if( changeOfType and this.IsLeading() )
      {
        traverse( this, Subscription, subscription, subscription.UseLeadingPeriod() )
        {
          LibCal_RecurrencePeriodWithEndDate::Create( subscription, startDate_i, endDate_i );
          subscription.IsChanged( true );
        }
      }
    
      // If applicable, register that a specific RecurrencePeriod has been created for the subscription.
      if( not this.IsLeading() )
      {
        this.UseLeadingPeriod( false );
      }
    }
  *]
}
