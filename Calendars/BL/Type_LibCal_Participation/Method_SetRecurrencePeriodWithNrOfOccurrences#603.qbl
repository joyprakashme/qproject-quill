Quintiq file version 2.0
#parent: #root
Method SetRecurrencePeriodWithNrOfOccurrences (Date startDate_i, Number nrOfOccurrences_i)
{
  Description: '[Deprecated] Set a recurrence Period that starts at a specific date and recurs for a specific number of times.'
  TextBody:
  [*
    // Check if a new RecurrencePeriod must be created.
    period       := guard( this.RecurrencePeriod().astype( LibCal_RecurrencePeriodNrOfOccurrences ), null( LibCal_RecurrencePeriodNrOfOccurrences ) );
    changeOfType := isnull( period );
    
    isChanged := changeOfType  // There was no period yet, or it was not a PeriodNrOfOccurrences
              or period.StartDate()       <> startDate_i
              or period.NrOfOccurrences() <> nrOfOccurrences_i;
    
    if( isChanged )
    {
      // Create the RecurrencePeriod.
      LibCal_RecurrencePeriodNrOfOccurrences::Create( this, startDate_i, nrOfOccurrences_i );
      this.IsChanged( true );
    
      // If there was a change in type of RecurrencePeriod, then also update the subscriptions.
      if( changeOfType and this.IsLeading() )
      {
        traverse( this, Subscription, subscription )
        {
          LibCal_RecurrencePeriodNrOfOccurrences::Create( subscription, startDate_i, nrOfOccurrences_i );
          subscription.IsChanged( true );
        }
      }
    
      // If applicable, register that a specific RecurrencePeriod has been created for the subscription.
      if( not this.IsLeading() )
      {
        this.UseLeadingPeriod( false );
      }
    }
  *]
  InterfaceProperties { Accessibility: 'Module' }
}
