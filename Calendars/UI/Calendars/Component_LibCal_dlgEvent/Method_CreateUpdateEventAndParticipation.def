Quintiq file version 2.0
#parent: #root
Method CreateUpdateEventAndParticipation () as LibCal_Participation id:Method_LibCal_dlgEvent_CreateUpdateEventAndParticipation
{
  #keys: '[131094.1.136167643]'
  Body:
  [*
    sEvent         := dhEvent.Data();
    sParticipation := Dialog.Data();
    
    // Get the values from the controls.
    
    // Event and Participation
    subject        := edtName.Text();
    type           := rbgEventType.BoundValue();
    capacity       := [Real]edtCapacity.Text();
    isDefault      := ckbIsDefault.Checked();
    startDate      := dtStartDate.DateTime().Date()
    startTimeOfDay := durStartTimeOfDay.Duration();
    endDate        := dtEndDate.DateTime().Date();
    endTimeOfDay   := durEndTimeOfDay.Duration();
    allDay         := ckbAllDay.Checked();
    
    // RecurrencePattern
    isRecurring := not btnNoRecurrence.Pressed();
    
    patternType := LibCal_RecurrencePattern::TYPE_NONE();
    if( btnWeekly.Pressed() )
    {
      patternType := LibCal_RecurrencePattern::TYPE_WEEKLY();
    }
    else if( btnMonthly.Pressed() )
    {
      patternType := LibCal_RecurrencePattern::TYPE_MONTHLY();
    }
    else if( btnYearly.Pressed() )
    {
      patternType := LibCal_RecurrencePattern::TYPE_YEARLY();
    }
    
    // Daily (deprecated)
    daily_IsEveryWeekday := false;
    
    // Weekly
    weekly_Weekdays := ifexpr( btnMonday   .Pressed(), "Mon;", "" )
                     + ifexpr( btnTuesday  .Pressed(), "Tue;", "" )
                     + ifexpr( btnWednesday.Pressed(), "Wed;", "" )
                     + ifexpr( btnThursday .Pressed(), "Thu;", "" )
                     + ifexpr( btnFriday   .Pressed(), "Fri;", "" )
                     + ifexpr( btnSaturday .Pressed(), "Sat;", "" )
                     + ifexpr( btnSunday   .Pressed(), "Sun;", "" );
    
    // Monthly
    monthly_IsDay       := ckbMonthlyDay.Checked();
    monthly_Day         := [Number]edtMonthlyDay.Text();
    monthly_WeekOfMonth := ssMonthlyPatternWeekOfMonth.BoundValue();
    monthly_DayOfWeek   := ssMonthlyPatternDayOfWeek.BoundValue();
    
    // Yearly
    yearly_IsDate      := ckbYearlyDate.Checked();
    month              := ifexpr( yearly_IsDate, ssYearlyMonth.BoundValue(), ssYearlyPatternMonth.BoundValue() );
    yearly_Month       := this.GetNrOfMonth( month );
    yearly_Day         := [Number]ssYearlyDayOfMonth.BoundValue();
    yearly_WeekOfMonth := ssYearlyPatternWeekOfMonth.BoundValue();
    yearly_DayOfWeek   := ssYearlyPatternDayOfWeek.BoundValue();
    
    // RecurrenceInterval
    recurrenceInterval := 1;
    if( patternType = LibCal_RecurrencePattern::TYPE_DAILY() )  // deprecated
    {
      recurrenceInterval := 1;
    }
    else if( patternType = LibCal_RecurrencePattern::TYPE_WEEKLY() )
    {
      recurrenceInterval := [Number]edtEveryNrOfWeeks.Text();
    }
    else if( patternType = LibCal_RecurrencePattern::TYPE_MONTHLY() )
    {
      recurrenceInterval := ifexpr( monthly_IsDay, [Number]edtMonthlyDayEveryNrOfMonths.Text(),
                                                   [Number]edtMonthlyPatternEveryNrOfMonths.Text() );
    }
    else if( patternType = LibCal_RecurrencePattern::TYPE_YEARLY() )
    {
      recurrenceInterval := [Number]edtEveryNrOfYears.Text();
    }
    
    // RecurrencePeriod
    periodType := ifexpr( ckbEndOfPeriod.Checked(), LibCal_RecurrencePeriod::TYPE_WITHENDDATE(),
                                                    LibCal_RecurrencePeriod::TYPE_WITHOUTEND() );
    
    periodStartDate := dtStartOfPeriod.DateTime().Date();
    periodEndDate   := dtEndOfPeriod.DateTime().Date();
    
    // All values are passed to the Server, only the relevant values are used there.
    participation := LibCal_Event::CreateUpdateFromUI( sEvent.Calendar(), sParticipation.WrappedInstance(),
                                                       subject, type, capacity, isDefault,
                                                       startDate, startTimeOfDay, endDate, endTimeOfDay, allDay,
                                                       // RecurrencePattern
                                                       isRecurring, recurrenceInterval, patternType,
                                                       daily_IsEveryWeekday,
                                                       weekly_Weekdays,
                                                       monthly_IsDay, monthly_Day,  monthly_WeekOfMonth, monthly_DayOfWeek,
                                                       yearly_IsDate, yearly_Month, yearly_Day, yearly_WeekOfMonth, yearly_DayOfWeek,
                                                       // RecurrencePeriod
                                                       periodType, periodStartDate, periodEndDate,
                                                       // Subscribers
                                                       dhSubscribers.Data() );
    return participation;
  *]
}
