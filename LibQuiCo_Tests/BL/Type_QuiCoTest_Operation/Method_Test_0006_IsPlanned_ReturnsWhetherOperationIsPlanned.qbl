Quintiq file version 2.0
#parent: #root
Method Test_0006_IsPlanned_ReturnsWhetherOperationIsPlanned
{
  Description: 'Tests that the IsPlanned constraint (used as an attribute) returns true when the operation is planned, and false when it is not planned.'
  TextBody:
  [*
    // Arrange
    this.GetCompanyData()->( ds ) 
    {
      operation := this.GetTestOperation( ds ) ;
      
      // Act
      isPlannedBeforePlan := operation.IsPlanned();
      operation.Plan();
      Transaction::Transaction().Propagate();
      isPlannedAfterPlan  := operation.IsPlanned();
      
      // Assert
      this.Run().AssertFalse( isPlannedBeforePlan, 'Expected that IsPlanned returns false for the unplanned operation.' );
      this.Run().AssertTrue( isPlannedAfterPlan,  'Expected that IsPlanned returns true for the planned operation.' );
          
    }->Exception()->( ex )
    {
      this.Run().RegisterFailure( ex.Message() );
    }
  *]
  InterfaceProperties { Accessibility: 'Module' }
}
